import Form from './Form';
import TextInput from "./TextInput";
import Checkbox from "./Checkbox";
import Button from "./Button";
import classes from '../styles/Signup.module.css';
import { Link,useNavigate  } from 'react-router-dom';
import { useAuth } from './contexts/Authcontext';
import { useState } from 'react';

export default function SignupForm(){

     const [name,setName] = useState('');
     const [email,setEmail] = useState('');
     const [password,setPassword] = useState('');
     const [confirmPassword,setConfirmPassword] = useState('');
     const [term,setTerm] = useState('');

     const [error,setError] = useState('');
     const [loading,setLoading] = useState(false);

     const {signup} = useAuth();
     const navigate = useNavigate();

     async function handleSubmit(e){
        e.preventDefault();
        if(password !== confirmPassword){
            setError('Password not match');
        }

        try{
            setError('');
            setLoading(true);
            await signup(email,password,name);   
            navigate('/');
        }catch(error){
            console.log(error);
            setLoading(false);
            setError('Signup Failed');
        }

     }
 

    return (
        <Form className={`${classes.signup}`} onSubmit={handleSubmit}> 
            <TextInput type="text" placeholder="Enter name" 
                value={name} onChange ={(e)=> setName(e.target.value)} 
                icon='person' /> 
            <TextInput type="text" placeholder="Enter email" 
                value={email} onChange ={(e)=> setEmail(e.target.value)}
                icon='alternate_email' /> 
            <TextInput type="password" placeholder="Enter password"
                 value={password} onChange ={(e)=> setPassword(e.target.value)} 
                icon='lock' /> 
            <TextInput type="password" placeholder="Confirm password" 
                value={confirmPassword} onChange ={(e)=> setConfirmPassword(e.target.value)}
                icon='lock_clock' /> 
            <Checkbox type="checkbox" 
                value={term} onChange ={(e)=> setTerm(e.target.value)}
                text="I agree to the Terms &Amp; Conditions"/> 
            <Button disabled={loading} type='submit'><span> Submit now </span></Button>
              {error && <p class="error">{error}</p>}
            <div className="info">
                 Already have an account? <Link to="/login">Login</Link> instead.
            </div>
       </Form>
    );
}