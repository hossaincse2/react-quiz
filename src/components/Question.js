import classes from '../styles/Question.module.css'
import Answer from "./Answer";

export default function Question(){
    return (
        <div className={classes.question}>
        <div className={classes.qtitle}>
          <span class="material-icons-outlined"> help_outline </span>
          Here goes the question from Learn with Sumit?
        </div>
        <Answer />
        {/* <div class="answers"> 
          <label class="answer" for="option1"> A New Hope 1 </label> 
        </div> */}
      </div>
    );
}