import classes from '../styles/Illustration.module.css';

export default function Illustration({ImageName}){
    return (
        <div className={classes.illustration}>
            <img src={ImageName} alt="Signup" />
         </div>
    );
}