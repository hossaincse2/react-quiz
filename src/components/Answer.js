import Checkbox from "./Checkbox";
import classes from '../styles/Answer.module.css';
export default function Answer(){
    return (
        <div class={classes.answers}> 
            <Checkbox className={classes.answer} type="checkbox" id="option1" text='A New Hope 1'  /> 
        </div>
    );
}