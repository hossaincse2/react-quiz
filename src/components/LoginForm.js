import Form from "./Form";
import TextInput from "./TextInput";
import Button from "./Button";
import classes from '../styles/Login.module.css'
import { Link,useNavigate  } from 'react-router-dom';
import { useAuth } from './contexts/Authcontext';
import { useState } from 'react';

export default function LoginForm(){ 
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');  
    const [error,setError] = useState(); 

    const {login} = useAuth();
    const navigate = useNavigate();

    async function handleSubmit(e){
       e.preventDefault(); 

       try{
           setError(''); 
           await login(email,password);   
           navigate('/');
       }catch(error){
           console.log(error); 
           setError('Login Failed');
       }

    }
    return (
        <Form className={classes.login} onSubmit={handleSubmit}>
                <TextInput type="text"  placeholder="Enter email"  
                 value={email} onChange ={(e)=> setEmail(e.target.value)} 
                 icon='alternate_email' /> 
                <TextInput type="password" 
                 value={password} onChange ={(e)=> setPassword(e.target.value)} 
                 placeholder="Enter password" icon='lock' /> 
                <Button type="submit">
                    <span> Submit now </span>
                </Button>
                {error && <p class="error">{error}</p>}
                <div className="info">Don't have an account? <Link to="/signup">Signup</Link> instead.</div>
        </Form>
    );
}